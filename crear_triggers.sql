create trigger cargar_alerta_rechazo_trg
after insert on rechazo
for each row
execute procedure cargar_alerta_rechazo();

create trigger cargar_alerta_compra_trg
after insert on compra
for each row
execute procedure cargar_alerta_compra();