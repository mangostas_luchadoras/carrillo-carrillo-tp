create or replace function cargar_alerta_compra() returns trigger as $$

declare
    nro_alert alerta.nroalerta%type;
    cod_alert alerta.codalerta%type;
    descrip alerta.descripcion%type;
    cant_comp_1 int;
    cant_comp_5 int;

begin
    select count(*) into cant_comp_1 from compra c1 where exists (
        (select 1 from comercio co where exists (
            select 1 where c1.nrotarjeta = new.nrotarjeta and co.nrocomercio <> new.nrocomercio
                and new.fecha - c1.fecha < '00:01:00' and c1.nrocomercio = co.nrocomercio  
                    and co.codigopostal = (select codigopostal from comercio com where com.nrocomercio = new.nrocomercio))));

    select count(*) into cant_comp_5 from compra c1 where exists (
        (select 1 from comercio co where exists (
            select 1 where c1.nrotarjeta = new.nrotarjeta and co.nrocomercio <> new.nrocomercio
                and new.fecha - c1.fecha < '00:05:00' and c1.nrocomercio = co.nrocomercio  
                    and co.codigopostal <> (select codigopostal from comercio com where com.nrocomercio = new.nrocomercio))));

    if cant_comp_1 >= 1 then
        cod_alert := 1;
        descrip := 'compra 1min';
        select count(*) into nro_alert from alerta;
        insert into alerta values(nro_alert, new.nrotarjeta, new.fecha, -1, cod_alert, descrip);
    end if;

    if cant_comp_5 >= 1 then
        cod_alert := 5;
        descrip := 'compra 5min';
        select count(*) into nro_alert from alerta;
        insert into alerta values(nro_alert, new.nrotarjeta, new.fecha, -1, cod_alert, descrip);
    end if;

    return new;
end;
$$ language plpgsql;
