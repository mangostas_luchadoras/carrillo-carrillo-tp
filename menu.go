package main

import (
    "fmt"
    L "./lib"
    "database/sql"
    _ "github.com/lib/pq"
    "log"
    "os"
    "time"
    "os/exec"
    "syscall"
)

var db *sql.DB
var historial string

func main() {
    System("clear")
    historial = ""

    db,err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    mostrarPresentacion()
    System("clear")

    fmt.Printf("\n\n")
    ejecutarMenu(db)  
}

func mostrarPresentacion(){
    fmt.Printf("                            `:-.    ./o+:                             	\n"+
    "                       `./hddmMMd-:hMMMMMm/                           	\n"+
    "                   `+mNMMMMMMMMo.hMMdyshNMMNy+-                       	\n"+
    "              `:oymMMMMMMMMMMM/-NNyomMMMMMMMMMMNdy+-`                 	\n"+
    "             :dMMMMMMMMMMMMMMs`NmymMMMMMMMMMMMMMMMMMMds:              	\n"+
    "          .odMMMMMMMMMMMMMMMM//Mh+mMMMMMMMMMMMMMNNMMMMMMNo`           	\n"+
    "        `+dMMMMMMMMMMMMMMMMMMh`hMhoydMMMMMMMMMMMMm+/yNMMMMm-          	\n"+
    "       `omMMMMMMMMMMMMMMMMMMMMmoohhmmNMMNNNMMMMMMMy   -sNMMN:         	\n"+
    "     `sNMMMMMMMMMMMMMMMMMMMMMMMMmNMMMMNNMMMMMMMMMMMNyo++oNMMN+        	\n"+
    "    /NMMMMMMMMMMMMMMMMMMMMMMMNssNMMNNNMMMMMMMMMMMMMMMMMMMMMMMMNs-     	\n"+
    "   ./hMMMMMMMMMMMMMMMMMMMMMMMmomMMNNMMMMMMMMMMMMMMMMMMMMMMMMMMMMMmy:  	\n"+
    "   :NMMMMMMMMMMMMMMMMMMMMMMMMd+NMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMhMM- 	\n"+
    "  :MMMMMMMMMMMMMMMMMMMMMMMMMMMoyMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMyNs  	\n"+
    " `ds+++++ooooooooshNMMMMMMMMMMMsomMMMMMMMMMMMMmyo+/+oshmMMMMMMMMdo`   	\n"+
    " `omMMMMMMMMMMMMMmho//ohNNMMMMMNNdhhmMMMMMMMM+          `-:hMh:`      	\n"+
    "  yMmyso+++osshdNMMMMNho:/yMMMMNo-..`+NMMMMMd            `sy-         	\n"+
    "  ..             `-/+yNMMmo-oNM-      -NMMMMh           `:`           	\n"+
    "                       ./yNNs-y        -NMMMm``                       	\n"+
    "                           -sd+         -mMMMm/.  .:`                 	\n"+
    "                              :+`         :yNMNoss/                   	\n"+
    "                                             -+o`  			\n"+
    "      	           _       __  __  ______ _   __    			\n"+
    "   	     /|/| /_| /| )/ _ /  )(   /  /_| (      			\n"+
    "   	    /   |(  |/ |/(__)(__/__) (  (  |__)      			\n"+
    "   	                                              			\n"+
    "                   _      _   __  __  __  _   __    			\n"+
    "           /  /  // ))__//_| /  )/  )/__)/_| (       			\n"+
    "          (__(__/(__/  /(  |/(_/(__// ( (  |__)      			\n"+
    "   	                                              			\n"+
    "   	        __  __  ___ _____   ____ _            			\n"+
    "   	       /__)/__)(_  ( (_  /| )/  /_|           			\n"+
    "   	      /   / (  /____)/__/ |/(  (  |          			\n")
    time.Sleep(3 * time.Second)
}

func imprimirMenu(){
    fmt.Printf("╔═════════════════════════════════════════════════════════════════╗\n" +
               "║    PRESIONE EL NÚMERO DE LA OPCIÓN Y 'ENTER' PARA CONTINUAR:    ║\n" +
               "╠═════════════════════════════════════════════════════════════════╣\n" + 
               "║  1. CREAR BASE DE DATOS: tarjetacredito.                        ║\n" +
               "║  2. CONECTARSE A BASE DE DATOS: tarjetacredito.                 ║\n" +
               "╠═════════════════════════════════════════════════════════════════╣\n" +
               "║  <<<IMPORTANTE>>> AL ABRIR EL MENÚ SE CONECTA A POSTGRES. PARA  ║\n" +
               "║    USAR LAS SIGUIENTES OPCIONES ES NECESARIO ESTAR CONETADO A   ║\n" +
               "║                LA BASE DE DATOS: tarjetacredito.                ║\n" +
               "╠═════════════════════════════════════════════════════════════════╣\n" + 
               "║  3. Crear tablas de datos.                                      ║\n" +  
               "║  4. Crear Primary Keys.                                         ║\n" +
               "║  5. Crear Foreing Keys.                                         ║\n" + 
               "║  6. Cargar clientes, comercios, cierres y consumos.             ║\n" +
               "║  7. Cargar Stored Procedures y Triggers.                        ║\n" +
               "║  8. Testear funcionalidades.                                    ║\n" +
               "║                                                                 ║\n" +
               "║  9. Borrar Primary & Foreing Keys.                              ║\n" +
               "╠═════════════════════════════════════════════════════════════════╣\n" + 
               "║  10. Cargar clientes, tarjetas, comercios y compras en BoltDB.  ║\n" +
               "║  11. Imprimir datos de BoltDB.                                  ║\n" +
               "╠═════════════════════════════════════════════════════════════════╣\n" +
               "║  12. Imprimir historial de ejecución.                           ║\n" +
               "║                                                                 ║\n" + 
               "║  0. Salir.                                                      ║\n" +
               "╠═════════════════════════════════════════════════════════════════╣\n" + 
               "║                   ¡NADA PUEDE MALIR SAL!                        ║\n" +
               "╚═════════════════════════════════════════════════════════════════╝\n")
}

func elegirOpcion(opcion int, db *sql.DB){
    switch opcion { 
        case 1: 
            L.CrearDatabase(db)
            System("clear")
            historial = historial + "SE CREO LA BASE DE DATOS: tarjetacredito.\n"
            fmt.Println("SE CREO LA BASE DE DATOS: tarjetacredito.\n") 
            ejecutarMenu(db)
        case 2:
            var err error
            db,err = sql.Open("postgres", "user=postgres host=localhost dbname=tarjetacredito sslmode=disable")
            if err != nil {
                log.Fatal(err)
            }
            defer db.Close() 
            System("clear")
            historial = historial + "SE CONECTO A BASE DE DATOS: tarjetacredito.\n"
            fmt.Println("SE CONECTO A BASE DE DATOS: tarjetacredito.\n")
            ejecutarMenu(db)
        case 3:
            L.CrearTables(db)
            System("clear")
            historial = historial + "SE CREARON LAS TABLAS DE DATOS.\n"
            fmt.Println("SE CREARON LAS TABLAS DE DATOS.\n")
            ejecutarMenu(db)
        case 4: 
            L.CrearPKs(db)
            System("clear")
            historial = historial + "SE CREARON LAS PRIMARY KEYS.\n"
            fmt.Println("SE CREARON LAS PRIMARY KEYS.\n")
            ejecutarMenu(db)
        case 5: 
            L.CrearFKs(db)
            System("clear")
            historial = historial + "SE CREARON LAS FOREIGN KEYS.\n"
            fmt.Println("SE CREARON LAS FOREIGN KEYS.\n")
            ejecutarMenu(db)
        case 6: 
            L.CargarClientes(db)
            L.CargarComercios(db)
            L.CargarCierres(db)
            L.CargarConsumos(db)
            System("clear")
            historial = historial + "SE CARGARON CLIENTES, COMERCIOS, CIERRES Y CONSUMOS.\n"
            fmt.Println("SE CARGARON CLIENTES, COMERCIOS, CIERRES Y CONSUMOS.\n")
            ejecutarMenu(db)
        case 7:
            L.CargarStoredProceduresAndTriggers(db)
            System("clear")
            historial = historial + "SE CARGARON STORED PROCEDURES Y TRIGGERS.\n"
            fmt.Println("SE CARGARON STORED PROCEDURES Y TRIGGERS.\n")
            ejecutarMenu(db)
        case 8:
            L.TestearFuncionalidades(db) 
            System("clear")
            historial = historial + "SE TESTEARON LAS FUNCIONALIDADES.\n"
            fmt.Println("SE TESTEARON LAS FUNCIONALIDADES.\n")
            ejecutarMenu(db)
        case 9: 
            L.BorrarKeys(db)
            System("clear")
            historial = historial + "SE BORRARON LAS PRIMARY & FOREIGN KEYS.\n"
            fmt.Println("SE BORRARON LAS PRIMARY & FOREIGN KEYS.\n")
            ejecutarMenu(db)
        case 10: 
            L.CargarBoltDB()
            System("clear")
            historial = historial + "SE CARGARON CLIENTES, TARJETAS, COMERCIOS Y COMPRAS EN BOLTDB.\n"
            fmt.Println("SE CARGARON CLIENTES, TARJETAS, COMERCIOS Y COMPRAS EN BOLTDB.\n")
            ejecutarMenu(db)   
        case 11: 
            System("clear")
            L.LeerBoltDB()
            historial = historial + "SE IMPRIMEN DATOS DE CLIENTES, TARJETAS, COMERCIOS Y COMPRAS EN BOLTDB.\n"
            ejecutarMenu(db)   
        case 12: 
            System("clear")
            fmt.Println(historial)
            historial = historial + "SE IMPRIME HISTORIAL DE EJECUCIÓN.\n"
            ejecutarMenu(db)  
        case 0:
            System("clear") 
            os.Exit(0)
        default:
            System("clear")   
            fmt.Println("ALGO MALIO SAL") 
    } 
}

func ejecutarMenu(db *sql.DB){
    var opcion int
    imprimirMenu()
    fmt.Scanf("%d", &opcion)
    
    elegirOpcion(opcion, db)
        
}

func System(cmd string) int {
    c := exec.Command("sh", "-c", cmd)
    c.Stdin = os.Stdin
    c.Stdout = os.Stdout
    c.Stderr = os.Stderr
    err := c.Run()

    if err == nil {
        return 0
    }

    // Figure out the exit code
    if ws, ok := c.ProcessState.Sys().(syscall.WaitStatus); ok {
        if ws.Exited() {
            return ws.ExitStatus()
        }

        if ws.Signaled() {
            return -int(ws.Signal())
        }
    }

    return -1
}
