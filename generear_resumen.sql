create or replace function generar_resumen(nro_client int, año_p int, mes_p int) returns void as $$

declare
	nro_tarj tarjeta.nrotarjeta%type;
	ter_tarj tarjeta.nrotarjeta%type;
	nro_resu cabecera.nroresumen%type;
	mont_tot cabecera.total%type;
	fila_det detalle.nrolinea%type;
	nomb_com comercio.nombre%type;
	client record;
	cier record;
	comp record;

begin
	select * into client from cliente where nrocliente = nro_client;

	for nro_tarj in select * from tarjeta t where t.nrocliente = nro_client and CURRENT_DATE < (t.validahasta || '01')::date loop	
		mont_tot := 0;
		select right(nro_tarj, 1) into ter_tarj;
		select count(*) into nro_resu from cabecera;
		select * into cier from cierre c where c.año = año_p and c.mes = mes_p and c.terminacion = ter_tarj::int; 
		insert into cabecera values(nro_resu, client.nombre, client.apellido, client.domicilio, nro_tarj, cier.fechainicio, cier.fechacierre, cier.fechavto, mont_tot);
	
		for comp in select * from compra c where c.nrotarjeta = nro_tarj and c.fecha >= cier.fechainicio and c.fecha <= cier.fechacierre and c.pagado = false loop
			select count(*) into fila_det from detalle d where d.nroresumen = nro_resu;
			select nombre into nomb_com from comercio c where c.nrocomercio = comp.nrocomercio;
			insert into detalle values(nro_resu, fila_det, comp.fecha, nomb_com, comp.monto);
			update compra set pagado = true where nrooperacion = comp.nrooperacion;
		end loop;

		select sum(monto) into mont_tot from detalle where nroresumen = nro_resu;
		if mont_tot is not null then
			update cabecera set total = mont_tot where nroresumen = nro_resu;
		end if;
	end loop;
end;
$$ language plpgsql;
