package lib

import (
    "encoding/json"
    "fmt"
    bolt "github.com/coreos/bbolt"
    "log"
    "strconv"
)


func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
    // abre transacción de escritura
    tx, err := db.Begin(true)
    if err != nil {
        return err
    }
    defer tx.Rollback()

    b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

    err = b.Put(key, val)
    if err != nil {
        return err
    }

    // cierra transacción
    if err := tx.Commit(); err != nil {
        return err
    }

    return nil
}

func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
    var buf []byte

    // abre una transacción de lectura
    err := db.View(func(tx *bolt.Tx) error {
        b := tx.Bucket([]byte(bucketName))
        buf = b.Get(key)
        return nil
    })

    return buf, err
}

func CargarBoltDB() {

    type Cliente struct {
        Nrocliente int
        Nombre string
        Apellido string
        Domicilio string
        Telefono string
    }

    type Tarjeta struct {
        Nrotarjeta int
        Nrocliente int
        Validadesde string
        Validahasta string
        Codseguridad int
        Limitecompra float64
        Estado string
    }
    
    type Comercio struct {
        Nrocomercio int
        Nombre string
        Domicilio string
        CodigoPostal int
        Telefono string
    }
	
	type Compra struct {
		Nrooperacion int
		Nrotarjeta int
		Nrocomercio int
		Fecha string
		Monto float64
		Pagado bool
	}
    
    var listaCliente [3]Cliente
    listaCliente[0] = Cliente {1, "Nicodemo", "Prieto", "Verapaz 1250, José C. Paz, Bs. As., Argrntina", "541134986495"}
    listaCliente[1] = Cliente {2, "Donato", "Vidal", "Tecigualpa 1600, José C. Paz, Bs. As., Argrntina", "541173953955"}
    listaCliente[2] = Cliente {3, "Gregorio", "Cruz", "Ruta 8 Km 41,5, José C. Paz, Bs. As., Argrntina", "541122786748"}

    var listaTarjeta [3]Tarjeta
    listaTarjeta[0] = Tarjeta {4357310922201904, 1,"2019-09", "2021-08", 575, 38000.00, "vigente"}
    listaTarjeta[1] = Tarjeta {4357315570417157, 2, "2019-07", "2021-06", 217, 21000.00, "vigente"}
    listaTarjeta[2] = Tarjeta {4357317291456993, 3, "2019-06", "2021-05", 781, 43000.00, "vigente"}

    var listaComercio [3]Comercio
    listaComercio[0] = Comercio {20,  "Sacabollo Gabriel", "Gral. J. A. Gelly y Obes 1943, El Talar, Bs. As., Argentina", 1618, "62157785"}
    listaComercio[1] = Comercio {19, "Panaderia Asturias", "L.M.Campos 1445, Don Torcuato, Bs. As., Argentina", 1611, "47481923"}
    listaComercio[2] = Comercio {18, "Carnicería El Reencuentro", "Balzac 2197, Pablo Nogués, Bs. As., Argentina", 1616, "1144137451"}

    var listaCompra [3]Compra
    listaCompra[0] = Compra {1, 4357315570417157, 20, "2020-06-03", 1346.5, false }
    listaCompra[1] = Compra {2, 4357310922201904, 19, "2020-06-02", 805.00, true}
    listaCompra[2] = Compra {3, 4357317291456993, 18, "2020-06-01", 3919.99, true}

    db, err := bolt.Open("tarjetaCredito.db", 0600, nil)
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    for i := 0; i < len(listaCliente); i++ {
        data, err := json.Marshal(listaCliente[i])
        if err != nil {
            log.Fatal(err)
        }
        CreateUpdate(db, "cliente", []byte(strconv.Itoa(listaCliente[i].Nrocliente)), data)
    }

    for i := 0; i < len(listaTarjeta); i++ {
        data, err := json.Marshal(listaTarjeta[i])
        if err != nil {
            log.Fatal(err)
        }
        CreateUpdate(db, "tarjeta", []byte(strconv.Itoa(listaTarjeta[i].Nrotarjeta)), data)
    }

    for i := 0; i < len(listaComercio); i++ {
        data, err := json.Marshal(listaComercio[i])
        if err != nil {
            log.Fatal(err)
        }
        CreateUpdate(db, "comercio", []byte(strconv.Itoa(listaComercio[i].Nrocomercio)), data)
    }

    for i := 0; i < len(listaCompra); i++ {
        data, err := json.Marshal(listaCompra[i])
        if err != nil {
            log.Fatal(err)
        }
        CreateUpdate(db, "compra", []byte(strconv.Itoa(listaCompra[i].Nrooperacion)), data)
    }
}

func LeerBoltDB() {

	db, err := bolt.Open("tarjetaCredito.db", 0600, nil)
    if err != nil {
        log.Fatal(err)
    }
	defer db.Close()
	
	for i := 1; i < 4; i++ {
		resultado, err := ReadUnique(db, "cliente", []byte(strconv.Itoa(i)))
		if err != nil {
            log.Fatal(err)
        }
        fmt.Printf("%s\n", resultado)
	}

	resultado0, err := ReadUnique(db, "tarjeta", []byte(strconv.Itoa(4357310922201904)))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s\n", resultado0)
	resultado1, err := ReadUnique(db, "tarjeta", []byte(strconv.Itoa(4357310922201904)))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s\n", resultado1)
	resultado2, err := ReadUnique(db, "tarjeta", []byte(strconv.Itoa(4357317291456993)))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s\n", resultado2)

	for i := 18; i < 21; i++ {
		resultado, err := ReadUnique(db, "comercio", []byte(strconv.Itoa(i)))
		if err != nil {
            log.Fatal(err)
        }
        fmt.Printf("%s\n", resultado)
	}
	
	for i := 1; i < 4; i++ {
		resultado, err := ReadUnique(db, "compra", []byte(strconv.Itoa(i)))
		if err != nil {
            log.Fatal(err)
        }
        fmt.Printf("%s\n", resultado)
	}
}
