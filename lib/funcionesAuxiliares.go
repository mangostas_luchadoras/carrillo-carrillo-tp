package lib

import (
    "encoding/json"
    "database/sql"
    _ "github.com/lib/pq"
    "time"
    "log"
    "io/ioutil"
    "math/rand"
    "strconv"
)

func CrearDatabase(db *sql.DB) {
    var err error
    _, err = db.Exec(`drop database if exists tarjetacredito`)
    if err != nil {
        log.Fatal(err)
    }

    _, err = db.Exec(`create database tarjetacredito`)
    if err != nil {
        log.Fatal(err)
    }
}

func CrearTables(db *sql.DB) {
    var err error
    _, err = db.Exec(`create table cliente (nrocliente int, nombre text, apellido text, domicilio text, telefono char(12));
                    create table tarjeta (nrotarjeta char(16), nrocliente int, validadesde char(6),
                        validahasta char(6), codseguridad char(4), limitecompra decimal(8,2), estado char(10));
                    create table comercio (nrocomercio int, nombre text, domicilio text,codigopostal char(8), telefono char(12));
                    create table compra (nrooperacion int, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), pagado boolean);
                    create table rechazo (nrorechazo int, nrotarjeta char(16), nrocomercio int, fecha timestamp, monto decimal(7,2), motivo text);
                    create table cierre (año int, mes int, terminacion int, fechainicio date, fechacierre date, fechavto date);
                    create table cabecera (nroresumen int, nombre text, apellido text, domicilio text, nrotarjeta char(16), desde date, hasta date,
                        vence date, total decimal(8,2));
                    create table detalle (nroresumen int, nrolinea int, fecha date, nombrecomercio text, monto decimal(7,2));
                    create table alerta (nroalerta int, nrotarjeta char(16), fecha timestamp, nrorechazo int, codalerta int, descripcion text);
                    create table consumo (nrotarjeta char(16), codseguridad char(4), nrocomercio int, monto decimal(7,2));`)
    if err != nil {
        log.Fatal(err)
    }
}

func CrearPKs(db *sql.DB){
    var err error
    _, err = db.Exec(`alter table cliente add constraint cliente_pk primary key (nrocliente);
                    alter table tarjeta add constraint tarjeta_pk primary key (nrotarjeta);
                    alter table comercio add constraint comercio_pk primary key (nrocomercio);
                    alter table compra add constraint compra_pk primary key (nrooperacion);
                    alter table rechazo add constraint rechazo_pk primary key (nrorechazo);
                    alter table cierre add constraint cierre_pk primary key (año, mes, terminacion);
                    alter table cabecera add constraint cabecera_pk primary key (nroresumen);
                    alter table detalle add constraint detalle_pk primary key (nroresumen, nrolinea);
                    alter table alerta add constraint alerta_pk primary key (nroalerta);`)
    if err != nil {
        log.Fatal(err)
    }
}

func CrearFKs(db *sql.DB){
    var err error
    _, err = db.Exec(`alter table tarjeta add constraint tarjeta_nrocliente_fk foreign key (nrocliente) references cliente (nrocliente);
                    alter table compra add constraint compra_nrocomercio_fk foreign key (nrocomercio) references comercio (nrocomercio);
                    alter table compra add constraint compra_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta (nrotarjeta);
                    alter table rechazo add constraint rechazo_nrocomercio_fk foreign key (nrocomercio) references comercio (nrocomercio);
                    alter table rechazo add constraint rechazo_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta (nrotarjeta);
                    alter table cabecera add constraint cabecera_nrotarjeta_fk foreign key (nrotarjeta) references tarjeta (nrotarjeta);
                    alter table detalle add constraint detalle_nroresumen_fk foreign key (nroresumen) references cabecera (nroresumen);`)
    if err != nil {
        log.Fatal(err)
    }
}

func BorrarKeys(db *sql.DB) {
    var err error
    _, err = db.Exec(`alter table tarjeta drop constraint if exists tarjeta_nrocliente_fk;
                    alter table compra drop constraint if exists compra_nrocomercio_fk;
                    alter table compra drop constraint if exists compra_nrotarjeta_fk;
                    alter table rechazo drop constraint if exists rechazo_nrocomercio_fk;
                    alter table rechazo drop constraint if exists rechazo_nrotarjeta_fk;
                    alter table cabecera drop constraint if exists cabecera_nrotarjeta_fk;
                    alter table detalle drop constraint if exists detalle_nroresumen_fk;
                    alter table alerta drop constraint if exists alerta_nrorechazo_fk ;`)
    if err != nil {
    log.Fatal(err)
    }

    _, err = db.Exec(`alter table cliente drop constraint if exists cliente_pk;
                    alter table tarjeta drop constraint if exists tarjeta_pk;
                    alter table comercio drop constraint if exists comercio_pk;
                    alter table compra drop constraint if exists compra_pk;
                    alter table rechazo drop constraint if exists rechazo_pk;
                    alter table cierre drop constraint if exists cierre_pk;
                    alter table cabecera drop constraint if exists cabecera_pk;
                    alter table detalle drop constraint if exists detalle_pk;
                    alter table alerta drop constraint if exists alerta_pk;`)
    if err != nil {
        log.Fatal(err)
    }
}

func CargarClientes(db *sql.DB) {
    var err error
    data1, err := ioutil.ReadFile("./jsons/clientes.json")
    if err != nil {
      log.Fatal(err)
    }
    
    data2, err := ioutil.ReadFile("./jsons/tarjetas.json")
    if err != nil {
      log.Fatal(err)
    }
    
    type Cliente struct {
        Nrocliente string
        Nombre string
        Apellido string
        Domicilio string
        Telefono string
    }

    type Tarjeta struct {
        Nrotarjeta string
        Validadesde string
        Validahasta string
        Codseguridad string
        Limitecompra string
        Estado string
    }

    var listaCliente []Cliente
    var listaTarjeta []Tarjeta
    
    err = json.Unmarshal(data1, &listaCliente)
    if err != nil {
        log.Fatalf("JSON unmarshaling failed: %s", err)
    }

    err = json.Unmarshal(data2, &listaTarjeta)
    if err != nil {
        log.Fatalf("JSON unmarshaling failed: %s", err)
    }

    auxCliente := ""
    auxTarjeta := ""
    
    for i := 0; i < len(listaCliente); i++ {
        auxCliente = auxCliente + "insert into cliente values (" + listaCliente[i].Nrocliente + ", '" + listaCliente[i].Nombre + "', '" + listaCliente[i].Apellido + "', '" + listaCliente[i].Domicilio + "', '" + listaCliente[i].Telefono + "');"
        auxTarjeta = auxTarjeta + "insert into tarjeta values (" + listaTarjeta[i].Nrotarjeta + ", '" + listaCliente[i].Nrocliente + "', '" + listaTarjeta[i].Validadesde + "', '" + listaTarjeta[i].Validahasta + "', '" + listaTarjeta[i].Codseguridad + "', '" + listaTarjeta[i].Limitecompra + "', '" + listaTarjeta[i].Estado + "');"
        
        if listaCliente[i].Nombre == "Inés" && listaCliente[i].Apellido == "Lorenzo" {
            auxTarjeta = auxTarjeta + "insert into tarjeta values (" + listaTarjeta[len(listaCliente)].Nrotarjeta + ", '" + listaCliente[i].Nrocliente + "', '" + listaTarjeta[len(listaCliente)].Validadesde + "', '" + listaTarjeta[len(listaCliente)].Validahasta + "', '" + listaTarjeta[len(listaCliente)].Codseguridad + "', '" + listaTarjeta[len(listaCliente)].Limitecompra + "', '" + listaTarjeta[len(listaCliente)].Estado + "');"
        }

        if listaCliente[i].Nombre == "Patricio" && listaCliente[i].Apellido == "Calvo" {
            auxTarjeta = auxTarjeta + "insert into tarjeta values (" + listaTarjeta[len(listaCliente) + 1].Nrotarjeta + ", '" + listaCliente[i].Nrocliente + "', '" + listaTarjeta[len(listaCliente) + 1].Validadesde + "', '" + listaTarjeta[len(listaCliente) + 1].Validahasta + "', '" + listaTarjeta[len(listaCliente) + 1].Codseguridad + "', '" + listaTarjeta[len(listaCliente) + 1].Limitecompra + "', '" + listaTarjeta[len(listaCliente) + 1].Estado + "');"
        }
    }

    _, err = db.Exec(auxCliente)
    if err != nil {
        log.Fatal(err)
    }
    
    _, err = db.Exec(auxTarjeta)
    if err != nil {
        log.Fatal(err)
    }
}

func CargarComercios(db *sql.DB) {
    var err error
    data, err := ioutil.ReadFile("./jsons/comercios.json")
    if err != nil {
      log.Fatal(err)
    }

    type Comercio struct {
        Nrocomercio string
        Nombre string
        Domicilio string
        CodigoPostal string
        Telefono string
    }

    var lista []Comercio
    
    err = json.Unmarshal(data, &lista)
    if err != nil {
        log.Fatalf("JSON unmarshaling failed: %s", err)
    }
    
    aux := ""

    for i := 0; i < len(lista); i++ {
        aux = aux + "insert into comercio values (" + lista[i].Nrocomercio + ", '" + lista[i].Nombre + "', '" + lista[i].Domicilio + "', '" + lista[i].CodigoPostal + "', '" + lista[i].Telefono + "');"
    }

    _, err = db.Exec(aux)
    if err != nil {
        log.Fatal(err)
    }
}

func CargarCierres(db *sql.DB) {
    var err error
    data, err := ioutil.ReadFile("./jsons/cierres.json")
    if err != nil {
      log.Fatal(err)
    }

    type Cierre struct {
        Año string
        Mes string
        Terminacion string
        FechaInicio string
        FechaCierre string
        FechaVto string
    }

    var lista []Cierre

    err = json.Unmarshal(data, &lista)
    if err != nil {
        log.Fatalf("JSON unmarshaling failed: %s", err)
    }
   
    aux := ""

    for i := 0; i < len(lista); i++ {
        aux = aux + "insert into cierre values (" + lista[i].Año + ", '" + lista[i].Mes + "', '" + lista[i].Terminacion + "', '" + lista[i].FechaInicio + "', '" + lista[i].FechaCierre + "', '" + lista[i].FechaVto + "');"
    }

    _, err = db.Exec(aux)
    if err != nil {
        log.Fatal(err)
    }    
}

func CargarConsumos(db *sql.DB) {
    var err error
    data, err := ioutil.ReadFile("./jsons/consumos.json")
    if err != nil {
      log.Fatal(err)
    }

    type Consumo struct {
        Nrotarjeta string
        Codseguridad string
        Nrocomercio string
        Monto string
    }

    var lista []Consumo

    err = json.Unmarshal(data, &lista)
    if err != nil {
        log.Fatalf("JSON unmarshaling failed: %s", err)
    }
   
    aux := ""

    for i := 0; i < len(lista); i++ {
        aux = aux + "insert into consumo values (" + lista[i].Nrotarjeta + ", '" + lista[i].Codseguridad + "', '" + lista[i].Nrocomercio + "', '" + lista[i].Monto + "');"
    }

    _, err = db.Exec(aux)
    if err != nil {
        log.Fatal(err)
    }    
}

func CargarStoredProceduresAndTriggers(db *sql.DB) {
    var err error
    _, err = db.Exec(`create or replace function autorizar_compra(nro_tar char, cod_seg char, nro_com int, saldo_op decimal) returns boolean as $$
                    declare
                        tarj record;
                        nro_rech int;
                        saldo_pend decimal;
                        nro_op int;
                        cant_compra int;
                    begin
                        select count(*) into cant_compra from compra where nrotarjeta = nro_tar and pagado = false;
                        if cant_compra > 0 then
                            select sum(monto) into saldo_pend from compra where nrotarjeta = nro_tar and pagado = false;
                        else
                            saldo_pend := 0;
                        end if;
                        select * into tarj from tarjeta where nrotarjeta = nro_tar;
                        if not found or tarj.estado = 'anulada' then
                            select count(*) into nro_rech from rechazo;
                            insert into rechazo values(nro_rech, nro_tar, nro_com, now(), saldo_op, '?tarjeta no válida ó no vigente.');
                            return false;
                        elsif tarj.codseguridad <> cod_seg then
                            select count(*) into nro_rech from rechazo;
                            insert into rechazo values(nro_rech, nro_tar, nro_com, now(), saldo_op, '?código de seguridad inválido.');
                            return false;
                        elsif saldo_pend + saldo_op > tarj.limitecompra then
                            select count(*) into nro_rech from rechazo;
                            insert into rechazo values(nro_rech, nro_tar, nro_com, now(), saldo_op, '?supera límite de tarjeta.');
                            return false;
                        elsif (tarj.validahasta || '01')::date < now() then
                            select count(*) into nro_rech from rechazo;
                            insert into rechazo values(nro_rech, nro_tar, nro_com, now(), saldo_op, '?plazo de vigencia expirado.');
                            return false;
                        elseif tarj.estado = 'suspendida' then
                            select count(*) into nro_rech from rechazo;
                            insert into rechazo values(nro_rech, nro_tar, nro_com, now(), saldo_op, '?la tarjeta se encuentra suspendida.');
                            return false;
                        else
                            select count(*) into nro_op from compra;
                            insert into compra values(nro_op, nro_tar, nro_com, now(), saldo_op, false);
                            return true;	
                        end if;
                    end;
                    $$ language plpgsql;`)
    if err != nil {
        log.Fatal(err)
    }  
    
    _, err = db.Exec(`create or replace function generar_resumen(nro_client int, año_p int, mes_p int) returns void as $$
                    declare
                        nro_tarj tarjeta.nrotarjeta%type;
                        ter_tarj tarjeta.nrotarjeta%type;
                        nro_resu cabecera.nroresumen%type;
                        mont_tot cabecera.total%type;
                        fila_det detalle.nrolinea%type;
                        nomb_com comercio.nombre%type;
                        client record;
                        cier record;
                        comp record;
                    begin
                        select * into client from cliente where nrocliente = nro_client;
                        for nro_tarj in select * from tarjeta t where t.nrocliente = nro_client and CURRENT_DATE < (t.validahasta || '01')::date loop	
                            mont_tot := 0;
                            select right(nro_tarj, 1) into ter_tarj;
                            select count(*) into nro_resu from cabecera;
                            select * into cier from cierre c where c.año = año_p and c.mes = mes_p and c.terminacion = ter_tarj::int; 
                            insert into cabecera values(nro_resu, client.nombre, client.apellido, client.domicilio, nro_tarj, cier.fechainicio, cier.fechacierre, cier.fechavto, mont_tot);
                            for comp in select * from compra c where c.nrotarjeta = nro_tarj and c.fecha >= cier.fechainicio and c.fecha <= cier.fechacierre and c.pagado = false loop
                                select count(*) into fila_det from detalle d where d.nroresumen = nro_resu;
                                select nombre into nomb_com from comercio c where c.nrocomercio = comp.nrocomercio;
                                insert into detalle values(nro_resu, fila_det, comp.fecha, nomb_com, comp.monto);
                                update compra set pagado = true where nrooperacion = comp.nrooperacion;
                            end loop;
                            select sum(monto) into mont_tot from detalle where nroresumen = nro_resu;
                            if mont_tot is not null then
                                update cabecera set total = mont_tot where nroresumen = nro_resu;
                            end if;
                        end loop;
                    end;
                    $$ language plpgsql;`)
    if err != nil {
        log.Fatal(err)
    }
    
    _, err = db.Exec(`create or replace function cargar_alerta_rechazo() returns trigger as $$
                    declare
                        nro_alert alerta.nroalerta%type;
                        cod_alert alerta.codalerta%type;
                        descrip alerta.descripcion%type;
                        cant_rech int;                    
                    begin
                        select count(*) into cant_rech from rechazo r where new.nrotarjeta = r.nrotarjeta and r.motivo = '?supera límite de tarjeta.' 
                        and (select extract (days from (new.fecha - r.fecha)::interval) = 0 where new.nrotarjeta = r.nrotarjeta and r.motivo = '?supera límite de tarjeta.');                        
                        if new.motivo = '?supera límite de tarjeta.' and cant_rech > 1 then
                            cod_alert := 32;
                            descrip := 'limite';
                            update tarjeta set estado = 'suspendida' where nrotarjeta = new.nrotarjeta;
                        else
                            cod_alert := 0;
                            descrip := 'rechazo';
                        end if;                            
                        select count(*) into nro_alert from alerta;
                        insert into alerta values(nro_alert, new.nrotarjeta, new.fecha, new.nrorechazo, cod_alert, descrip);                        
                        return new;
                    end;
                    $$ language plpgsql;`)
    if err != nil {
        log.Fatal(err)
    }

    _, err = db.Exec(`create or replace function cargar_alerta_compra() returns trigger as $$
                    declare
                        nro_alert alerta.nroalerta%type;
                        cod_alert alerta.codalerta%type;
                        descrip alerta.descripcion%type;
                        cant_comp_1 int;
                        cant_comp_5 int;                    
                    begin
                        select count(*) into cant_comp_1 from compra c1 where exists (
                            (select 1 from comercio co where exists (
                                select 1 where c1.nrotarjeta = new.nrotarjeta and co.nrocomercio <> new.nrocomercio
                                    and new.fecha - c1.fecha < '00:01:00' and c1.nrocomercio = co.nrocomercio  
                                        and co.codigopostal = (select codigopostal from comercio com where com.nrocomercio = new.nrocomercio))));                    
                        select count(*) into cant_comp_5 from compra c1 where exists (
                            (select 1 from comercio co where exists (
                                select 1 where c1.nrotarjeta = new.nrotarjeta and co.nrocomercio <> new.nrocomercio
                                    and new.fecha - c1.fecha < '00:05:00' and c1.nrocomercio = co.nrocomercio  
                                        and co.codigopostal <> (select codigopostal from comercio com where com.nrocomercio = new.nrocomercio))));                    
                        if cant_comp_1 >= 1 then
                            cod_alert := 1;
                            descrip := 'compra 1min';
                            select count(*) into nro_alert from alerta;
                            insert into alerta values(nro_alert, new.nrotarjeta, new.fecha, -1, cod_alert, descrip);
                        end if;                    
                        if cant_comp_5 >= 1 then
                            cod_alert := 5;
                            descrip := 'compra 5min';
                            select count(*) into nro_alert from alerta;
                            insert into alerta values(nro_alert, new.nrotarjeta, new.fecha, -1, cod_alert, descrip);
                        end if;                    
                        return new;
                    end;
                    $$ language plpgsql;`)
    if err != nil {
        log.Fatal(err)
    }

    _, err = db.Exec(`create trigger cargar_alerta_rechazo_trg
                    after insert on rechazo
                    for each row
                    execute procedure cargar_alerta_rechazo();`)
    if err != nil {
        log.Fatal(err)
    }

    _, err = db.Exec(`create trigger cargar_alerta_compra_trg
                    after insert on compra
                    for each row
                    execute procedure cargar_alerta_compra();`)
    if err != nil {
        log.Fatal(err)
    }
}

func TestearFuncionalidades(db *sql.DB) {  
    var err error  
    type Consumo struct {
        Nrotarjeta string
        Codseguridad string
        Nrocomercio string
        Monto string
    }
    
    rows, err := db.Query(`select * from consumo`)
    if err != nil {
        log.Fatal(err)
    }
    defer rows.Close()

    var c Consumo
    for rows.Next() {
        n := rand.Intn(3) + 1
        time.Sleep(time.Duration(n) * time.Second)

        if err := rows.Scan(&c.Nrotarjeta, &c.Codseguridad, &c.Nrocomercio, &c.Monto); err != nil {
            log.Fatal(err)
        }

        aux := "select autorizar_compra('" + c.Nrotarjeta + "', '" + c.Codseguridad + "', '" + c.Nrocomercio + "', '" + c.Monto + "');"

        _, err = db.Exec(aux)
        if err != nil {
            log.Fatal(err)
        }
    }

    rows1, err := db.Query(`select nrocliente from cliente`)
    if err != nil {
        log.Fatal(err)
    }
    defer rows1.Close()

    var nrocliente string
    year, month, _ := time.Now().Date()
    mes := monthToInt(month)
    
    for rows1.Next() {

        if err := rows1.Scan(&nrocliente); err != nil {
            log.Fatal(err)
        }

        aux := "select generar_resumen(" + nrocliente + ", '" + strconv.Itoa(year) + "', '" + strconv.Itoa(mes) + "');"

        _, err = db.Exec(aux)
        if err != nil {
            log.Fatal(err)
        }
    }
}

func monthToInt(m time.Month) int {
    var ret int
    switch m {
        case time.January:
            ret = 1
        case time.February:
            ret = 2
        case time.March:
            ret = 3
        case time.April:
            ret = 4
        case time.May:
            ret = 5
        case time.June:
            ret = 6
        case time.July:
            ret = 7
        case time.August:
            ret = 8
        case time.September:
            ret = 9
        case time.October:
            ret = 10
        case time.November:
            ret = 11
        case time.December:
            ret = 12
        default:
            ret = 0
    }

    return ret
}

