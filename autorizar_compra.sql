create or replace function autorizar_compra(nro_tar char, cod_seg char, nro_com int, saldo_op decimal) returns boolean as $$

declare
	tarj record;
	nro_rech int;
	saldo_pend decimal;
	nro_op int;
	cant_compra int;

begin
	select count(*) into cant_compra from compra where nrotarjeta = nro_tar and pagado = false;
	if cant_compra > 0 then
		select sum(monto) into saldo_pend from compra where nrotarjeta = nro_tar and pagado = false;
	else
		saldo_pend := 0;
	end if;
	select * into tarj from tarjeta where nrotarjeta = nro_tar;	

	if not found or tarj.estado = 'anulada' then
		select count(*) into nro_rech from rechazo;
		insert into rechazo values(nro_rech, nro_tar, nro_com, now(), saldo_op, '?tarjeta no válida ó no vigente.');
		return false;
	elsif tarj.codseguridad <> cod_seg then
		select count(*) into nro_rech from rechazo;
		insert into rechazo values(nro_rech, nro_tar, nro_com, now(), saldo_op, '?código de seguridad inválido.');
		return false;
	elsif saldo_pend + saldo_op > tarj.limitecompra then
		select count(*) into nro_rech from rechazo;
		insert into rechazo values(nro_rech, nro_tar, nro_com, now(), saldo_op, '?supera límite de tarjeta.');
		return false;
	elsif (tarj.validahasta || '01')::date < now() then
		select count(*) into nro_rech from rechazo;
		insert into rechazo values(nro_rech, nro_tar, nro_com, now(), saldo_op, '?plazo de vigencia expirado.');
		return false;
	elseif tarj.estado = 'suspendida' then
		select count(*) into nro_rech from rechazo;
		insert into rechazo values(nro_rech, nro_tar, nro_com, now(), saldo_op, '?la tarjeta se encuentra suspendida.');
		return false;
	else
		select count(*) into nro_op from compra;
		insert into compra values(nro_op, nro_tar, nro_com, now(), saldo_op, false);
		return true;	
	end if;
end;
$$ language plpgsql;
