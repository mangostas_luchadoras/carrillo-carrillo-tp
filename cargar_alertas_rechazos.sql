create or replace function cargar_alerta_rechazo() returns trigger as $$

declare
    nro_alert alerta.nroalerta%type;
    cod_alert alerta.codalerta%type;
    descrip alerta.descripcion%type;
    cant_rech int;

begin
    select count(*) into cant_rech from rechazo r where new.nrotarjeta = r.nrotarjeta and r.motivo = '?supera límite de tarjeta.' 
        and (select extract (days from (new.fecha - r.fecha)::interval) = 0 where new.nrotarjeta = r.nrotarjeta and r.motivo = '?supera límite de tarjeta.');
    
    if new.motivo = '?supera límite de tarjeta.' and cant_rech > 1 then
        cod_alert := 32;
        descrip := 'limite';
        update tarjeta set estado = 'suspendida' where nrotarjeta = new.nrotarjeta;
    else
        cod_alert := 0;
        descrip := 'rechazo';
    end if;    
    
    select count(*) into nro_alert from alerta;
    insert into alerta values(nro_alert, new.nrotarjeta, new.fecha, new.nrorechazo, cod_alert, descrip);
    
    return new;
end;
$$ language plpgsql;