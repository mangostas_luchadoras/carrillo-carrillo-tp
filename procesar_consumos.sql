create or replace function procesar_consumos() returns boolean as $$
declare
	v record;
begin
	for v in select * from consumo loop
		perform autorizar_compra(v.nrotarjeta, v.codseguridad, v.nrocomercio, v.monto);
	end loop;
	return true;
end;
$$ language plpgsql;
